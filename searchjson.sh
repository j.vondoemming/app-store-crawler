#!/bin/bash

set -e




JSON="$(cat clean.json | jq '.')"
CUR="."
# Search For
#SF="/store/apps/details?id="

# This function should be a simple check that is used to cut off large portions of the json tree drastically reducing the search time.
# If this function returns false, then the entire sub-path given via "${1}" is ignored.
# 
# Arguments:
# "${1}": current path in "${JSON}"
simple_check_func() {
	#[ "$(echo "${JSON}" | jq "${1}" | grep -F "${SF}")" != "" ]; return
	[ "$(echo "${JSON}" | jq "${1}" | grep '\"Apps\"' | grep '\"#text\"')" != "" ]; return
}

# If this function returns true, then the search is over and "${1}" is the resulting path.
# Note: This function has a higher priority than simple_check_func()!
# 
# Arguments:
# "${1}": current path in "${JSON}"
final_check_func() {
	#[ "$(echo "${JSON}" | jq "${1}" | grep -F "${SF}")" != "" ]; return
#	simple_check_func "${1}" && echo "${1}" | grep '\"h2\"\.\"#text\"$' ; return
	echo "${JSON}" | jq -e "${1} | has(\"h2\")" 2>/dev/null || return
	echo "${JSON}" | jq -e "${1}.h2 | has(\"#text\")" || return
	[ "$(echo "${JSON}" | jq -r "${1}.h2.\"#text\"")" = "Apps" ] && return
}


jsonsearch() {
	local lcur="${1}"
	echo "lcur: \"${lcur}\""
	if final_check_func "${lcur}"; then
		true; return
	fi
	if simple_check_func "${lcur}"; then
		local lcurtype="$(echo "${JSON}" | jq -r "${lcur} | type")"
		echo "lcurtype: \"${lcurtype}\""
		CUR="${lcur}"
		
		if [ "${lcurtype}" = "array" ]; then
			local lcurlen="$(echo "${JSON}" | jq -r "${lcur} | length")"
			echo "lcurlen: \"${lcurlen}\""
			#echo "${JSON}" | jq -r "${lcur} | keys"
			local i
			for ((i = 0 ; i < ${lcurlen} ; i++)); do
				echo "Number: $i"
				jsonsearch "${lcur}[${i}]" && return
			done
			
		elif [ "${lcurtype}" = "object" ]; then
			local lkeys="$(echo "${JSON}" | jq -r "${lcur} | keys")"
			local lkeyslen="$(echo "${lkeys}" | jq -r ". | length")"
			echo "lkeyslen: \"${lkeyslen}\""
			local i
			local lkeycur
			for ((i = 0 ; i < ${lkeyslen} ; i++)); do
				echo "Number: $i"
				lkeycur="$(echo "${lkeys}" | jq -r ".[${i}]")"
				jsonsearch "${lcur}.\"${lkeycur}\"" && return
			done

	#	elif [ "${lcurtype}" = "string" ]; then
	#		# FOUND
	#		true; return
			
		else
			echo "ERROR: Unknown type: \"${CURTYPE}\""
			echo "${JSON}" | jq -r "${lcur}"
			exit 1
		fi
	fi
	echo "Nothing found!"
	false; return
}

jsonsearch "${CUR}" || exit 1
echo "FOUND!"
echo "CUR: \"${CUR}\""
echo "${JSON}" | jq -r "${CUR}"

echo -n "${CUR}" > jsonsearchresult.txt
