# App Store Crawler

Tool um Ergebnisse aus der Google Play Store Suche als JSON auf der Command-Line zu bekommen.

## WARNUNG

DIESES REPO WURDE IN NUR EINER NACHT ZUSAMMENGEHACKT UND DER CODE HIER DRIN IST ZIEMLICH HÄSSLICH!

SOLLTEST DU HIER PER ZUFALL DRAUFGESTOßEN SEIN, EMPFEHLE ICH DEN TAB DIREKT WIEDER ZU SCHLIEßEN!

ES KANN SEHR GUT SEIN, DASS NICHTS IN DIESEM REPO FUNKTIONIERT!

## Abhängigkeiten

- bash
- GNU Coreutils
- firefox
- jq
- python3
- pip3
- wget
- curl
- tar

und die [Abhängigkeiten die sich tidy-html5 wünscht](tidy-html5/README/BUILD.md).

## Klonen und Aufsetzen

```
git clone --recurse-submodules https://gitlab.gwdg.de/j.vondoemming/app-store-crawler.git
cd app-store-crawler
./setup.sh
```

Note: setup.sh installiert einige Software via pip3 lokal, also im Homeverzeichnis.

## Config

Ähm...ja... Da ist ne `Config`-Sektion in der [run.sh](run.sh)...

Eigentlich muss man das aber auch nicht editieren.

Ich hab ja gesagt, dass der Code nicht schön ist.

## Anwendung

```
Usage: ./run.sh <Search Query>
```

Beispiel:
```
./run.sh "Privacy"
```

Zum Schluss, sofern alles geklappt hat, sollte da sowas stehen wie:
```


==> Finished! <==
Started: Sa 25. Sep 11:02:43 CEST 2021
Ended: Sa 25. Sep 11:04:03 CEST 2021

A total of 248 apps were found
for the query "Privacy".

Results can be found in result/result.json !
(The other stuff in result/ might also be useful.)
```

Note: Die .txt Dateien in `result/` sind **NICHT** miteinander kombinierbar!

## Getestet

Entwickelt und getestet auf den CIP-Pool Rechnern der Informatik am 25.09.2021. Keine Ahnung ob das Ganze auch irgendwo anders laufen kann.
