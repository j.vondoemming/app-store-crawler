from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import time
import urllib
import sys


def scrolltobottom(driver, waittime = 3):
    print("scrolling to bottom... (waittime: " + str(waittime) + "seconds)")
    pageyoffset = driver.execute_script("return window.pageYOffset;");
    oldpageyoffset = pageyoffset + 1;
    print("yoffset: " + str(pageyoffset));
    while pageyoffset != oldpageyoffset:
        oldpageyoffset = pageyoffset
        time.sleep(waittime)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        pageyoffset = driver.execute_script("return window.pageYOffset;");
        print("yoffset: " + str(pageyoffset));
    print("scrolled to bottom.")

# returns html of full site
def getplaystoresearchresults(query, timeout = 10, scrollwaittime = 3, headless = True, closebrowserwhendone = True, category = "apps"):
    options = FirefoxOptions()
    if headless:
        options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)
    driver.implicitly_wait(timeout)
    url = 'https://play.google.com/store/search?q=' + urllib.parse.quote(query, safe='') + '&c=' + category
    print("Url: " + url)
    driver.get(url)
    html = None
    try:
        w = WebDriverWait(driver, timeout)
        w.until(EC.presence_of_element_located((By.TAG_NAME,"body")))
        print("Page loaded")
        scrolltobottom(driver, scrollwaittime)
        html = driver.page_source
    except TimeoutException:
        print("Timeout happened no page load")
    except Exception as e:
        print("Some general Exception happened:")
        print(e)

    if closebrowserwhendone or headless:
        driver.close()
        driver.quit()
    return html

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")


def printusage(scriptname):
    print("Usage: python3 " + scriptname + " <query> <timeout> <scrollwaittime> <headless> <closebrowserwhendone> <outputfile>")
    print("Args:")
    print("  - query [String]:")
    print("      Search Query")
    print("  - timeout [Num]:")
    print("      Timeout in Seconds (max page load time)")
    print("      Recommended Value: 10")
    print("  - scrollwaittime [String]:")
    print("      Waittime in Seconds between every scroll attempt")
    print("      (increase if slow pc/internet)")
    print("      Recommended Value: 3")
    print("  - headless [Bool]:")
    print("      Whether Firefox should be opened in headless mode")
    print("      (implies closebrowserwhendone = true)")
    print("      Recommended Value: True")
    print("  - closebrowserwhendone [Bool]:")
    print("      Whether Firefox should be closed after everything is done")
    print("      Recommended Value: True")
    print("  - outputfile [String]:")
    print("      Path to the output file for the HTML of the result")
    print("      Recommended Value: index.html")

if __name__ == '__main__':
    if len(sys.argv) != 7:
        printusage(sys.argv[0])
        raise ValueError('Wrong number of arguments')
    query = sys.argv[1]
    timeout = int(sys.argv[2])
    scrollwaittime = int(sys.argv[3])
    headless = str2bool(sys.argv[4])
    closebrowserwhendone = str2bool(sys.argv[5])
    outputfile = sys.argv[6]
    res = getplaystoresearchresults(query, timeout = timeout, scrollwaittime = scrollwaittime, headless = headless, closebrowserwhendone = closebrowserwhendone)
    f = open(outputfile, "w")
    f.write(res)
    f.close()
    print("Success! Output written to: " + outputfile)

