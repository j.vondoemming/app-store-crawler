#!/bin/bash

set -e

if [ "$#" != "1" ]; then
	echo "Usage: $0 <Search Query>"
	exit 1
fi

################################### CONFIG ################################################

#  - query [String]:
#      Search Query
QUERY="${1}"
#  - timeout [Num]:
#      Timeout in Seconds (max page load time)
#      Recommended Value: 10
TIMEOUT="10"
#  - scrollwaittime [String]:
#      Waittime in Seconds between every scroll attempt
#      (increase if slow pc/internet)
#      Recommended Value: 3
SCROLLWAITTIME="3"
#  - headless [Bool]:
#      Whether Firefox should be opened in headless mode
#      (implies closebrowserwhendone = true)
#      Recommended Value: True
HEADLESS="True"
#  - closebrowserwhendone [Bool]:
#      Whether Firefox should be closed after everything is done
#      Recommended Value: True
CLOSEBROWSERWHENDONE="True"
#  - outputfile [String]:
#      Path to the output file for the HTML of the result
#      Recommended Value: index.html
PLAYSTOREFILE="index.html"

################################### CONFIG ################################################

STARTTIME="$(date)"

export PATH="${PATH}:$(pwd -P)/bin/"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$(pwd -P)/lib/"

echo "Note: Using Search Query: ${QUERY}"

echo ""
echo "=> Creating tmp directory..."
echo ""

mkdir -p "tmp"
pushd "tmp"

echo ""
echo "=> Getting raw HTML Search Result from Google Play Store..."
echo ""

python3 ../getplaystoresearchresults.py "${QUERY}" "${TIMEOUT}" "${SCROLLWAITTIME}" "${HEADLESS}" "${CLOSEBROWSERWHENDONE}" "${PLAYSTOREFILE}"

echo ""
echo "=> Tidying HTML..."
echo ""

set +e
cat "${PLAYSTOREFILE}" | tidy -w 0 -u -ashtml -utf8 --force-output yes > clean.html

echo ""
echo "=> Tidying HTML and converting to XML..."
echo ""

cat "${PLAYSTOREFILE}" | tidy -w 0 -u -asxml --output-xml yes -utf8 --force-output yes > clean.xml
set -e


echo -n "${QUERY}" > query.txt
##cat clean.html | grep "href=\"/store/apps/details.*</A></DIV>$" | tr '"' '\n' | grep "/store/apps/details" | sed 's,^,https://play.google.com,' > urls.txt
cat clean.html | grep "href=\"/store/apps/details" | tr '"' '\n' | grep "/store/apps/details" | sort -u | sed 's,^,https://play.google.com,' > urls.txt

if [ "$(cat urls.txt)" = "" ]; then
	echo ""
	echo ""
	echo ""
	echo ""
	echo "==> Finished! <=="
	echo "Started: ${STARTTIME}"
	echo "Ended: $(date)"
	echo ""
	echo "No apps were found"
	echo "for the query \"${QUERY}\"."

	exit 0
fi

cat urls.txt | cut -d '=' -f 2 > ids.txt
cat clean.html | grep "/store/apps/details?id=" -A 3  | grep "title=\"" | tr '"' '\n' | grep 'title=$' -A 1 | grep -v 'title=$' | grep -v '^--$' > titles.txt

echo ""
echo "=> Fixing Edge Cases in XML..."
echo ""

sed -i 's,&nbsp;, ,g' clean.xml

echo ""
echo "=> Converting XML to JSON..."
echo ""

cat clean.xml | xq '.html.body.div' > clean.json

echo ""
echo "=> Reverse engineering obfuscated layout..."
echo ""

../searchjson.sh

JSONSEARCHRESULT="$(cat jsonsearchresult.txt)"
PATHALLELEMENTS="$(echo "${JSONSEARCHRESULT}" | sed 's,\[0\]$,[1].div,')"
echo "PATHALLELEMENTS: \"${PATHALLELEMENTS}\""
cat clean.json | jq "${PATHALLELEMENTS}" > allelements.json
JSONALLELEMENTS="$(cat allelements.json | jq '.')"
JSONALLELEMENTSLEN="$(echo "${JSONALLELEMENTS}" | jq -r '. | length')"

echo ""
echo "=> Creating result.json ..."
echo ""

echo "" > result.json
echo "{" >> result.json
echo "\"settings\": {" >> result.json
ESCAPEDQUERY="$(echo -n "${QUERY}" | python3 -c "import urllib.parse; print(urllib.parse.quote(input()))")"
echo "\"query\":\"${ESCAPEDQUERY}\"," >> result.json
echo "\"timeout\":\"${TIMEOUT}\"," >> result.json
echo "\"scrollwaittime\":\"${SCROLLWAITTIME}\"," >> result.json
echo "\"headless\":\"${HEADLESS}\"," >> result.json
echo "\"closebrowserwhendone\":\"${CLOSEBROWSERWHENDONE}\"" >> result.json
echo "}, " >> result.json

echo "Number of apps: ${JSONALLELEMENTSLEN}"

echo "\"apps\": [" >> result.json

ISFIRST="1"
for ((i = 0 ; i < ${JSONALLELEMENTSLEN} ; i++)); do
	echo "App: $(( $i + 1 )) / ${JSONALLELEMENTSLEN}"
	CUR="$(echo "${JSONALLELEMENTS}" | jq ".[${i}]")"
	echo "${CUR}" > curraw.json
	#echo "${CUR}" | jq -e '.[0]' | grep developer && echo "ERROR: developer found!" && exit 1
	#echo "CUR: \"${CUR}\""
	CURCOMPANYNAME="$(echo "${CUR}" | grep -e '\"#text\"' -e '\"/store/apps/developer?id=' | grep '\"/store/apps/developer?id=' -A 1 | grep '\"#text\"' | cut -d '"' -f 4- | sed 's/,$//g;s/\"$//g' | head -n 1)"
	CURCOMPANYURL="https://play.google.com$(echo "${CUR}" | grep '\"/store/apps/developer?id=' | cut -d '"' -f 4 | head -n 1)"
	if [ "$CURCOMPANYNAME" = "" ]; then
		CURCOMPANYNAME="$(echo "${CUR}" | grep -e '\"#text\"' -e '\"/store/apps/dev?id=' | grep '\"/store/apps/dev?id=' -A 1 | grep '\"#text\"' | cut -d '"' -f 4- | sed 's/,$//g;s/\"$//g' | head -n 1)"
		CURCOMPANYURL="https://play.google.com$(echo "${CUR}" | grep "\"/store/apps/dev?id=" | cut -d '"' -f 4 | head -n 1)"
		
	fi
	CURAPPURL="https://play.google.com$(echo "${CUR}" | grep '\"/store/apps/details?id=' | cut -d '"' -f 4 | head -n 1)"
	CURAPPID="$(echo "${CURAPPURL}" | cut -d '=' -f 2)"
	CURTITLE="$(echo "${CUR}" | grep '\"@title\"' | cut -d '"' -f 4- | sed 's/,$//g;s/\"$//g' | head -n 1)"
	CURRATING="$(echo "${CUR}" | grep "\"Rated " | cut -d '"' -f 4- | sed 's/,$//g;s/\"$//g' | head -n 1)"
	CURIMG="$(echo "${CUR}" | grep "\"@src\"" | cut -d '"' -f 4 | head -n 1)"
	echo "{" > cur.json
	echo "\"id\":\"${CURAPPID}\"," >> cur.json
	echo "\"url\":\"${CURAPPURL}\"," >> cur.json
	echo "\"title\":\"${CURTITLE}\"," >> cur.json
	echo "\"img\":\"${CURIMG}\"," >> cur.json
	if [ "${CURRATING}" != "" ]; then
		echo "\"rating\":\"${CURRATING}\"," >> cur.json
	fi
	echo "\"developer\": {" >> cur.json
	echo "\"name\":\"${CURCOMPANYNAME}\"," >> cur.json
	echo "\"url\":\"${CURCOMPANYURL}\"" >> cur.json
	echo "}" >> cur.json
	echo "}" >> cur.json
	
	cat cur.json | jq '.'

	if  [ "${ISFIRST}" = "1" ]; then
		ISFIRST="0"
	else
		echo "," >> result.json
	fi
	cat cur.json >> result.json
done

echo "]" >> result.json

echo "}" >> result.json

popd

echo ""
echo "=> Copying results..."
echo ""

mkdir -p result

cat tmp/result.json | jq '.' > result/result.json
cp -vf tmp/ids.txt result/
cp -vf tmp/query.txt result/
cp -vf tmp/titles.txt result/
cp -vf tmp/urls.txt result/



echo ""
echo ""
echo ""
echo ""
echo "==> Finished! <=="
echo "Started: ${STARTTIME}"
echo "Ended: $(date)"
echo ""
echo "A total of ${JSONALLELEMENTSLEN} apps were found"
echo "for the query \"${QUERY}\"."
echo ""
echo "Results can be found in result/result.json !"
echo "(The other stuff in result/ might also be useful.)"

