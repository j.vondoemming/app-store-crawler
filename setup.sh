#!/bin/bash

set -e

#[ ! -f geckodriver.zip ] && wget https://hg.mozilla.org/mozilla-central/archive/tip.zip/testing/geckodriver/ -O geckodriver.zip
#unzip -u geckodriver.zip
#ln -fvs mozilla-central-*/testing/geckodriver/ geckodriver

mkdir -p bin
pushd bin
[ ! -f geckodriver.tar.gz ] && wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz -O geckodriver.tar.gz
tar xvzf geckodriver.tar.gz
popd

pip3 install -U selenium
pip3 install -U yq

pushd tidy-html5
cd build/cmake
#cmake ../.. -DCMAKE_BUILD_TYPE=Release [-DCMAKE_INSTALL_PREFIX=/path/for/install]
cmake ../.. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../..
make
make install
#[sudo] make install
popd
